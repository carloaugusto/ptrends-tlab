clear
close all
clc

fname = uigetfile('*.csv', 'Load .csv file');
Tab = readtable(fname);
date_format = 'yyyy-MM-dd''T''HH';

time_temp = cell2mat(Tab.Tempo);
time = datetime(time_temp, 'InputFormat', date_format);
% compute sampling period
Ts = hours(time(2)-time(1));

variables = Tab.Properties.VariableNames;
n_samples = size(Tab.(1), 1);
M = 5; %oversampling
N0 = n_samples*M;
v0 = [(0:1:N0/2-1), (-N0/2:1:-1)]/(N0*Ts);
            
n_porn = size(variables, 2)-1;

fh1 = figure(1); % time plot
for p = 1 : n_porn
    plot(time, Tab.(p+1), 'LineWidth', 2)
    hold on
end
xlabel('Day')
ylabel('Normalized queries')
grid on
legend(variables{2:end},'Location','southoutside','orientation','horizontal')
legend('boxoff')
set(gca, 'FontSize', 30, 'TickDir', 'out');
fh1.WindowState = 'maximized';
pbaspect([2 1 1])
print('fig/anal1.eps','-depsc')

fh2 = figure(2); % spectrum plot
for p = 1 : n_porn
    porn_q = Tab.(p+1);
    mean_porn = mean(porn_q);
    XXX = fft(porn_q-mean_porn, N0)/n_samples;
    plot(v0(1:N0/2), abs(XXX(1:N0/2)), 'LineWidth', 2)
    hold on
end
xlabel('Frequency [h^{-1}]')
ylabel('Amplitude')
grid on
legend(variables{2:end},'Location','southoutside','orientation','horizontal')
legend('boxoff')
set(gca, 'FontSize', 30, 'TickDir', 'out');
fh2.WindowState = 'maximized';
pbaspect([2 1 1])
print('fig/anal2.eps','-depsc')

pause(2)
close all